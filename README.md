# Mesamatrix

This repo is an attempt to re-implement of the current [Mesamatrix](https://github.com/MightyCreak/mesamatrix)
written in PHP.

## Repository structure

### Application source code

All the source code for the application is in the `src/` folder.

The application is split in two projects:

* Mesamatrix.Api
* Mesamatrix.Application

#### API project

The API project is a thin layer taking the REST requests and sending them to the Application
project using [MediatR](https://github.com/jbogard/MediatR).

The mediator pattern ensure a good separation between the requests and their execution.

The `Controllers/` folder is where all the controllers are.

The `Program.cs` file is the entry point to the whole application, it is where everything starts.

#### Application project

The Application project is where things are done. This is where actions related to Mesamatrix are
defined (i.e. queries and commands).

### Application tests

All the source code for the tests is in the `tests/` folder.

## Reference links

* [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)

## Developing with Gitpod

This template repository also has a fully-automated dev setup for [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html).

The `.gitpod.yml` ensures that, when you open this repository in Gitpod, you'll get a cloud
workspace with .NET pre-installed, and your project will automatically be built and start running.
