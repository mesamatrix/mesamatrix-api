using MediatR;
using Mesamatrix.Application.Commands;
using Microsoft.AspNetCore.Mvc;

namespace Mesamatrix.Api.Controllers;

[ApiController]
[Route("api")]
public class MesamatrixController : ControllerBase
{
    private readonly ISender _sender;

    public MesamatrixController(ISender sender)
    {
        _sender = sender;
    }

    [HttpPost("setup")]
    public async Task<ActionResult> Setup(CancellationToken cancellationToken)
    {
        var command = new SetupCommand();

        await _sender.Send(command, cancellationToken);

        return Ok();
    }

    [HttpPost("fetch")]
    public async Task<ActionResult> Fetch(CancellationToken cancellationToken)
    {
        var command = new FetchCommand();

        await _sender.Send(command, cancellationToken);

        return Ok();
    }

    [HttpPost("parse")]
    public async Task<ActionResult> Parse(CancellationToken cancellationToken)
    {
        var command = new ParseCommand();

        await _sender.Send(command, cancellationToken);

        return Ok();
    }
}
